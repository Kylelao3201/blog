$(function () {
    var APPLICATION_ID = "42C6A656-A980-DB95-FFD1-381D4DCF4200",
        SECRET_KEY = "8E2E3D71-6473-849C-FF3A-428F08C00800",
        VERSION = "v1";
    
    Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);
    
    //This  is to fix login problem. Uncomment line below and comment out the auto-login and run then reverse and it will work
   // Backendless.UserService.logout(new Backendless.Async(userLoggedOut, gotError));
    
        if(Backendless.UserService.isValidLogin()){
        userLoggedIn(Backendless.LocalCache.get("current-user-id"));
        } else {           
            var loginScript = $("#login-template").html();
            var loginTemplate = Handlebars.compile(loginScript);  
            $('.main-container').html(loginTemplate);
        }
    $(document).on('submit', '.form-signin', function(event){
        event.preventDefault();
        
        var  data = $(this).serializeArray(),
        email = data[0].value,
        password = data[1].value;
        
        Backendless.UserService.login(email, password, true, new Backendless.Async(userLoggedIn, gotError));
    });   
    
    $(document).on('click', '.add-blog', function(){
        var addBlogScript = $("#add-blog-template").html();
        var addBlogTemplate = Handlebars.compile(addBlogScript);
        $('.main-container').html(addBlogTemplate);  
        tinymce.init({ selector:'textarea' });
    });
    $(document).on('submit', '.form-add-blog', function(event){
        event.preventDefault();
        var data = $(this).serializeArray(),
            title = data[0].value,
            content = data[1].value;
            if(content === "" || title === "")
            {
            Materialize.toast("Please fill both title and content to continue", 1500);    
            }
            else
            {
                var dataStore = Backendless.Persistence.of(Posts);
        
        var postObject = new Posts({
            title: title,
            content: content,
            authorEmail: Backendless.UserService.getCurrentUser().email
        });
        Materialize.toast("Your post has been sent! Go to the main page to see it!", 2500);
            }
        
        
        dataStore.save(postObject);
        
        this.title.value = "";
        this.content.value = "";
    });
    $(document).on('click', '.logout', function (){
        Backendless.UserService.logout(new Backendless.Async(userLoggedOut, gotError));
        
        var loginScript = $("#login-template").html();
        var loginTemplate = Handlebars.compile(loginScript);  
        $('.main-container').html(loginTemplate);
    });
});

function Posts(args)
{
    args = args || {};
    this.title = args.title || "";
    this.content = args.content || "";
    this.authorEmail = args.authorEmail || "";
}

function userLoggedIn(user)
{
    console.log("user succsessfully logged in");
    var userData;
    if(typeof user == "string"){
        userData = Backendless.Data.of(Backendless.User).findById(user);
    }
    else
    {
        userData = user;
    }
       
    var welcomeScript = $('#welcome-template').html();
    var welcomeTemplate = Handlebars.compile(welcomeScript);
    var welcomeHTML = welcomeTemplate(userData);
    
    $('.main-container').html(welcomeHTML);
}
function userLoggedOut()
{
    console.log("successfully logged out");
}

function gotError(error)
{
    console.log("Error message -" + error.message);
    console.log("Error code - " + error.code);
    Materialize.toast('WRONG USERNAME OR PASSWORD!', 1000);
}
